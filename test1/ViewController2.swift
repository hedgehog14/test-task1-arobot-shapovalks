//
//  ViewController2.swift
//  test1
//
//  Created by Kiryl Shapaval on 25/01/2019.
//  Copyright © 2019 Kiryl Shapaval. All rights reserved.
//

import UIKit

class ViewController2: UIViewController {
    
    var taskVC = Task()
    var lableName = UILabel()
    var labelDisc = UILabel()

    override func viewDidLoad() {
        super.viewDidLoad()

          self.title = taskVC.name

        let labelDiscFrame = CGRect(x: 0, y: 0, width: 100, height: 50)
        labelDisc.frame = labelDiscFrame
        labelDisc.font = UIFont.boldSystemFont(ofSize: 18)
        labelDisc.textColor = UIColor.black
        labelDisc.center = self.view.center
        labelDisc.text = taskVC.descriptionTask
        self.view.addSubview(labelDisc)
        
    }
 
    

}
