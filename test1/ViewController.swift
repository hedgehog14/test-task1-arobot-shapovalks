//
//  ViewController.swift
//  test1
//
//  Created by Kiryl Shapaval on 25/01/2019.
//  Copyright © 2019 Kiryl Shapaval. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    var testTableView = UITableView()
    let identifire = "TestCell"
    var taskCell : [Task] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Поручения"
        createTable()
        let tasks = Task()
        tasks.name = "Task 1"; tasks.descriptionTask = "Wash a car"
        taskCell.append(tasks)
        testTableView.reloadData()
    }
    func createTable () {
        self.testTableView = UITableView(frame: view.bounds, style: .plain)
        testTableView.register(CustomCell.self, forCellReuseIdentifier: identifire)
        self.testTableView.delegate = self
        self.testTableView.dataSource = self
        testTableView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        view.addSubview(testTableView)
        testTableView.reloadData()
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.taskCell.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TestCell") as! CustomCell
        let task = self.taskCell[indexPath.row]
        cell.nameTitle.text = task.name
        cell.nameDisc.text = task.descriptionTask
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80.0
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let selectedCell = taskCell[indexPath.row]
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        var vc = storyboard.instantiateViewController(withIdentifier: "VC2") as! ViewController2
        vc.taskVC = selectedCell
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
