//
//  CustomCell.swift
//  test1
//
//  Created by Kiryl Shapaval on 25/01/2019.
//  Copyright © 2019 Kiryl Shapaval. All rights reserved.
//

import UIKit

class CustomCell: UITableViewCell {
   
     var nameTitle: UILabel!
     var nameDisc: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super .init(style: style, reuseIdentifier: "TestCell")
        nameTitle = UILabel.init(frame: CGRect(x: 10, y: 15, width: 150, height: 50))
        nameTitle.text = "Поручение"
        nameTitle.font = UIFont.boldSystemFont(ofSize: 18)
        nameTitle.textColor = .black
        addSubview(nameTitle)
        nameDisc = UILabel.init(frame: CGRect(x: 200, y: 15, width: 200, height: 50))
        nameDisc.text = "Починить машину"
        nameDisc.font = UIFont.boldSystemFont(ofSize: 18)
        nameDisc.textColor = .black
        addSubview(nameDisc)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

     
    }

}
